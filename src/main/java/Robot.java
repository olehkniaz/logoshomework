public class Robot {
    private String name ;
    private String action ;

    public Robot(String name,String action) {
        this.name = name;
        this.action = action;
    }
    public void work(){
        System.out.println("I " + name + " just "+ action);
    }
}
